import { Example } from '../index';

test('Example', () => {
  expect(Example('Bob')).toBe('Hello Bob!');
});
