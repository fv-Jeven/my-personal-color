# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- .prettierignore

## [v2.0.1] - 2023-07-10

- Fix GitLab pipeline

## [v2.0.0] - 2023-07-10

### Added

- Prettier is also formatting `.json` and `.yml` files
- JSDoc is globally enforced, so we cannot sneak around the documentation of the code

### Changed

- Updated lock file version from `1` to `2`
- Upgraded node version from `12` to `20`
- Upgraded dependencies

## [v1.0.3]

### Changed

- replaced badge source fury.io with shields.io

## [v1.0.2]

### Added

- badge for npm version
- badge for bundle size

## [v1.0.1]

### Added

- gitlab-ci
- Credits for gitlab-ci

### Changed

- Typo

## [v1.0.0]

### Added

- Initial release

[unreleased]: https://gitlab.com/wanjapflueger/npm-example
[v2.0.1]: https://gitlab.com/wanjapflueger/npm-example/-/compare/v2.0.1...v2.0.0
[v2.0.0]: https://gitlab.com/wanjapflueger/npm-example/-/compare/v2.0.0...v1.0.2
[v1.0.2]: https://gitlab.com/wanjapflueger/npm-example/-/compare/v1.0.2...v1.0.1
[v1.0.1]: https://gitlab.com/wanjapflueger/npm-example/-/compare/v1.0.1...v1.0.0
[v1.0.0]: https://gitlab.com/wanjapflueger/npm-example/-/tree/v1.0.0
